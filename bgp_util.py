import re
from datetime import datetime
import subprocess
import os
import logging
import pdb
from glob import glob

import psycopg2

from util import daterange


class BGPmessage():
    def __init__(self, text=None):
        self.message_dict = {}
        if text:
            self.parse(text)


    def parse(self, text):
        if 'IPv6 Unicast' in text: return
        one_line = re.compile('(\S*?):(.*)')
        self.text = text
        in_multiline = False
        for line in text.split('\n'):
            one_liner = one_line.match(line)
            if one_liner:
                # One line declaration, finish multiline if previous
                if in_multiline:
                    self.message_dict[variable] = values
                    in_multiline = False

                variable = one_liner.group(1)
                value = one_liner.group(2).strip()
                self.message_dict[variable] = value 

            elif not in_multiline:
                # Start of multiliner
                variable = line
                values = []
                in_multiline = True

            else: 
                # Parsing multiline values
                if line == line.lstrip():
                    # New multiline
                    self.message_dict[variable] = values
                    variable = line
                    values = []
                else:
                    values.append(line.strip())

        if in_multiline:
            # Finish multiline
            self.message_dict[variable] = values


    def __str__(self):
        return self.text


    def __getitem__(self, name):
        return self.message_dict[name]


    def has_key(self, name):
        return self.message_dict.has_key(name)


    def get(self, name, default=None):
        return self.message_dict.get(name, default)


    def __contains__(self, key):
        return self.message_dict.__contains__(key)



def BGPDumpParser(text):
    for message_body in text.split('\n\n'):
        yield BGPmessage(message_body)


class Prefix():
    def __init__(self, text):
        ip, masklen = text.split('/')
        self.ip = [int(el) for el in ip.split('.')]
        self.masklen = int(masklen)


    def covers(self, prefix):
        n = int(self.masklen/8)
        m = self.masklen % 8
        covers = (self.ip[:n] == prefix.ip[:n]) and \
                 (self.ip[n] >> (8-m) == prefix.ip[n] >> (8-m))
        return covers


    def __str__(self):
        return '.'.join([str(num) for num in self.ip])+'/'+str(self.masklen)

    def __eq__(self, prefix):
        return self.ip == prefix.ip and self.masklen == prefix.masklen


def extract_announce_paths(message):
    '''
    Make announce path records.
    '''
    time = datetime.strptime(message['TIME'], "%m/%d/%y %H:%M:%S")
    prefixes = message['ANNOUNCE']
    return ({'prefixes':prefixes, 
             'aspath':message['ASPATH'].split(), 
             'datetime':time})

class QueueStop():
    def __init__(self, num_workers=None):
        self.num_workers = num_workers
        self.num_workers_done = 0
        
    def tag(self):
        self.num_workers_done += 1

    def all_done(self):
        return self.num_workers_done == self.num_workers


def bgpfile_parser(file_queue, output_queue, parse):
    '''
    Takes a bgp update filename and puts aspath info.
    '''
    while True:
        item = file_queue.get()
        if isinstance(item, QueueStop):
            item.tag()    
            if item.all_done():
                output_queue.put(QueueStop())
            else:
                file_queue.put(item)
            break
        logging.info('(PID:%d)Dumping: %s' % (os.getpid(), item))
        bgpdump = subprocess.Popen(['bgpdump', item], stdout=subprocess.PIPE)
        dump, err = bgpdump.communicate()
        for message in BGPDumpParser(dump):
            if message.has_key('ANNOUNCE'):
                output_queue.put(parse(message))


def announce_path_writer(record_queue):
    '''
    Write announce path records in queue to database.
    '''
    #connection = psycopg2.connect(host='prometheus.icasa.nmt.edu', database='bwallin', user='bwallin')
    connection = psycopg2.connect(database='bwallin', user='bwallin')
    #connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = connection.cursor()
    while True:
        item = record_queue.get()
        if isinstance(item, QueueStop):
            item.tag()
            record_queue.put(item)
            connection.commit()
            break
        cursor.execute('INSERT INTO announced_paths VALUES(%s::cidr[], %s, %s)',
                       (item['prefixes'], item['aspath'], item['datetime']))
    

def bgpfile_generator(start, end):
    #base_dir = '/LocalData/InternetProblem/raw'
    base_dir = '/netdata/Support/InternetProblem/raw'
    for date in daterange(start, end):
        year, month, day = date.year, date.month, date.day
        files = glob(os.path.join(base_dir,'{0}/{1:02d}/{2:02d}/*ripe0_upd.gz'.format(year, month, day)))
        for file in files:
            yield file.strip()


def queue_logger(queue, interval=60):
    import time
    while True:
       time.sleep(interval)
       logging.info('Queue %d' % queue.qsize())
