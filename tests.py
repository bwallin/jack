import unittest
import decay
from datetime import datetime, timedelta

class TestDecayingMarkovChain(unittest.TestCase):
    def setUp(self):
        self.path_list = (((1, 3), datetime(2100, 1, 1, 0, 0, 0)), 
                          ((3, 5), datetime(2100, 1, 1, 0, 0, 0)),
                          ((1, 3), datetime(2100, 1, 1, 1, 0, 0)),
                          ((3, 5), datetime(2100, 1, 1, 1, 0, 0)), 
                          ((1, 3), datetime(2100, 1, 1, 2, 0, 0)), 
                          ((3, 4), datetime(2100, 1, 1, 2, 0, 0)), 
                          ((4, 5), datetime(2100, 1, 1, 2, 0, 0)),
                          ((2, 4), datetime(2100, 1, 1, 3, 0, 0)),
                          ((4, 5), datetime(2100, 1, 1, 3, 0, 0)),
                          ((1, 2), datetime(2100, 1, 1, 5, 0, 0)),
                          ((2, 3), datetime(2100, 1, 1, 5, 0, 0)))

    def testDecay(self):
        chain = decay.DecayingMarkovChain(halflife=timedelta(minutes=1))
        for (edge, edge_time) in self.path_list:
            chain.count_edge(edge, edge_time)

        chain.update(datetime(2100, 1, 1, 0, 1, 0))
        self.assertAlmostEqual(chain[1][3], .5)
        self.assertAlmostEqual(chain[3][5], .5)
        chain.update(datetime(2100, 1, 1, 0, 2, 0))
        self.assertAlmostEqual(chain[1][3], .25)
        self.assertAlmostEqual(chain[3][5], .25)

    def testLearningWithDecay(self):
        chain = decay.DecayingMarkovChain(halflife=timedelta(hours=1))
        for (edge, edge_time) in self.path_list:
            chain.count_edge(edge, edge_time)

        self.assertEqual(chain.number_of_nodes(), 0)

        chain.update(datetime(2100, 1, 1, 0, 0, 0))
        self.assertEqual(chain.number_of_nodes(), 3)
        self.assertEqual(chain[1][3], 1)
        self.assertEqual(chain[3][5], 1)

        chain.update(datetime(2100, 1, 1, 1, 0, 0))
        self.assertEqual(chain.number_of_nodes(), 3)
        self.assertAlmostEqual(chain[1][3], 1.5)
        self.assertAlmostEqual(chain[3][5], 1.5)

        chain.update(datetime(2100, 1, 1, 2, 0, 0))
        self.assertEqual(chain.number_of_nodes(), 4)
        self.assertAlmostEqual(chain[1][3], 1.75)
        self.assertAlmostEqual(chain[3][5], .75)
        self.assertAlmostEqual(chain[3][4], 1)
        self.assertAlmostEqual(chain[4][5], 1)

        chain.update(datetime(2100, 1, 1, 3, 0, 0))
        self.assertEqual(chain.number_of_nodes(), 5)
        self.assertAlmostEqual(chain[1][3], .875)
        self.assertAlmostEqual(chain[3][5], .375)
        self.assertAlmostEqual(chain[3][4], .5)
        self.assertAlmostEqual(chain[2][4], 1)
        self.assertAlmostEqual(chain[4][5], 1.5)



if __name__=='__main__':
    unittest.main()
