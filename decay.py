from datetime import timedelta
import itertools
from collections import defaultdict
import logging
import pdb

from scipy import exp, log, floor, nan
from scipy.misc import comb
#import networkx as nx


class ShortTermMemory():
    def __init__(self, duration):
        self.memory = dict()
        self.duration = duration
    
    def update(self, time):   
        self.memory = dict((k,v) for (k,v) in self.memory.iteritems() if time-v <= self.duration)
    
    def learn(self, item, time):
        self.update(time)     
        self.memory[item] = time

    def __contains__(self, item):
        return item in self.memory

    def __iter__(self):
        return iter(self.memory)

    def __len__(self):
        return len(self.memory)


class DecayingMean():
    def __init__(self, halflife):
        self.sum = 0 
        self.total_weight = 0
        self.timestamp = None
        self.halflife = halflife

    def add(self, value, time):
        self.update_decay(time)
        self.sum += value
        self.total_weight += 1

    def update_decay(self, time):
        if self.timestamp is None:
            decay = 1
        else:
            delta_seconds = (time - self.timestamp).seconds
            decay = exp(-log(2)/self.halflife*delta_seconds)
        self.sum *= decay
        self.total_weight *= decay
        self.timestamp = time

    def mean(self):
        return self.sum/self.total_weight


#class DecayingMarkovDiGraph(nx.DiGraph):
#    '''
#    A graph where edge weights decay exponentially over time.
#    '''
#    def __init__(self, halflife, learn_delay=timedelta(0)):
#        self.halflife = halflife
#        self.learn_delay = learn_delay
#        nx.DiGraph.__init__(self)
#
#        self._pending_edge_counts = []
#        self.timestamp = None
#
#    def count_edge(self, edge, time):
#        '''
#        Count edge (no effect until after learning delay).
#        '''
#        self._pending_edge_counts.append((time, edge))
#
#    def update(self, time):
#        if self.timestamp is not None:
#            assert time > self.timestamp
#        self.learn_edges(time)
#        self.decay_weights(time)
#
#    def learn_edges(self, time):
#        '''
#        Learn edges in queue if old enough.
#        '''
#        pending_edge_times = [row[0] for row in self._pending_edge_counts]
#        l = bisect(pending_edge_times, time-self.learn_delay)
#        for i in xrange(l):
#            edge_time, edge = self._pending_edge_counts[i]
#            self.decay_weights(edge_time)
#            n1, n2 = edge
#            if edge in self.edges():
#                self[n1][n2]['weight'] = self[n1][n2]['weight'] + 1
#            else:
#                self.add_edge(n1, n2, weight=1)
#        self._pending_edge_counts = self._pending_edge_counts[l:]
#
#    def decay_weights(self, time):
#        '''
#        Update edge weights to time by decaying and learning.
#        '''
#        if self.timestamp is None:
#            decay = 1
#        else:
#            delta_seconds = (time - self.timestamp).seconds
#            decay = exp(-log(2)/self.halflife*delta_seconds)
#        for n1, n2 in self.edges():
#            self[n1][n2]['weight'] = self[n1][n2]['weight']*decay
#
#        self.timestamp = time
#
#    def path_loglike(self, path, time, pseudocounts=1):
#        '''
#        Markov chain loglikelihood (uniform initial node likelihood)
#        '''
#        loglike = -log(len(self))
#        for k in xrange(len(path)-1):
#            a, b = path[k], path[k+1]
#            if (a,b) in self.edges():
#                weight = self[a][b]['weight'] + pseudocounts
#                total_weight = sum([self[x][y]['weight'] for x, y in self.edges(a)]) + len(self)*pseudocounts
#            else:
#                weight = pseudocounts
#                total_weight = len(self)*pseudocounts
#            loglike += log(weight) - log(total_weight)
#        
#        return loglike
#
#    def null_path_loglike(self, path):
#        '''
#        Likelihood of random path on lattice between 2 random nodes.
#        '''
#        null_loglike = -log(len(self)) - log(len(self) - 1) - \
#                        log(comb(len(path), floor(len(path)/2)))
#        return null_loglike


class DecayingMarkovChain(defaultdict):
    '''
    A markov chain where edge weights decay exponentially over time.
    '''
    def __init__(self, halflife, learn_delay=timedelta(0)):
        self.halflife_seconds = (halflife.microseconds + (halflife.seconds + halflife.days * 24 * 3600) * 10**6) / 10**6
        self.node_set = set()
        defaultdict.__init__(self, lambda: defaultdict(int))

        self._pending_edge_weights = [] # List of (time, edge, weight) tuples
        self.timestamp = None

    def weight_edge(self, edge, weight, time):
        '''
        Count edge (no effect until learn_edges called).
        '''
        self._pending_edge_weights.append((time, edge, weight))

    def update(self, time):
        if self.timestamp is not None:
            assert time > self.timestamp
        self.learn_edges(time)
        self.decay_weights(time)

    def learn_edges(self, time):
        '''
        Learn edges in queue.
        '''
        edges_to_learn = filter(lambda x: x[0] <= time, self._pending_edge_weights)        
        self._pending_edge_weights = filter(lambda x: x[0] > time, self._pending_edge_weights)
        for (edge_time, edge, weight) in edges_to_learn:
            self.node_set.update(edge)
            self.decay_weights(edge_time, min_decay=.99)
            n1, n2 = edge
            self[n1][n2] = self[n1][n2] + weight

    def decay_weights(self, time, min_decay=None):
        '''
        Update edge weights to time by decaying and learning.
        '''
        if self.timestamp is None:
            # First Update
            self.timestamp = time
            return
        elif min_decay is not None:
            # Skip update if will decay at least min_decay 
            min_timedelta = timedelta(seconds=self.halflife_seconds*log(min_decay)/-log(2))
            if (time - self.timestamp) < min_timedelta:
                return
        # Decay each edge
        delta_seconds = (time - self.timestamp).seconds
        decay = exp(-log(2)*delta_seconds/self.halflife_seconds)
        for origin, edge_dict in self.iteritems():
            for dest, counts in self[origin].iteritems():
                self[origin][dest] = counts*decay

        self.timestamp = time

    def number_of_nodes(self):
        return len(self.node_set)
    
    def number_of_edges(self):
        return sum(len(el) for el in self.items())

    def path_loglike(self, path, time, pseudoweight):
        '''
        Markov chain loglikelihood (uniform initial node likelihood)
        '''
        return markov_chain_loglike(self, dict(itertools.izip(self.node_set, itertools.repeat(1))),
                                    path, pseudoweight)
                                    
    def null_path_loglike(self, path):
        '''
        Likelihood of random path on lattice between 2 random nodes.
        '''
        return uniform_grid_loglike(self, dict(itertools.izip(self.node_set, itertools.repeat(1))), path)


class DiscreteUniform():
    def __init__(self, n):
        self.n = n

    def __getitem__(self, key):
        return 1./self.n if self.n > 0 else nan

    def __len__(self):
        return self.n


def markov_chain_loglike(edge_dict, init_dist, path, pseudoweight):
    '''
    Markov chain loglikelihood
    '''
    num_nodes = len(init_dist)
    loglike = log(init_dist[path[0]])
    for k in xrange(len(path)-1):
        n1, n2 = (path[k], path[k+1])
        if n1 in edge_dict and n2 in edge_dict[n1]:
            weight = edge_dict[n1][n2] + pseudoweight
            total_weight = sum(edge_dict[n1].values()) + num_nodes*pseudoweight
        else:
            weight = pseudoweight
            total_weight = num_nodes*pseudoweight
        loglike += log(weight) - log(total_weight)
    
    return loglike


def uniform_grid_loglike(edge_dict, init_dist, path):
    '''
    Likelihood of random path on grid between 2 random nodes.
    '''
    num_nodes = len(init_dist)
    null_loglike = -log(num_nodes) - log(num_nodes - 1) - \
                    log(comb(len(path), floor(len(path)/2)))
    return null_loglike
