import logging
import cProfile
import cPickle
from optparse import OptionParser
import pdb
from datetime import timedelta
import dateutil
import dateutil.parser
from collections import defaultdict
import multiprocessing
from itertools import izip, repeat, chain

import psycopg2
from progressbar import ProgressBar, ETA, Bar, Percentage, Timer
from scipy import zeros, floor

from misc_util import remove_duplicates, divtd
from decay import ShortTermMemory, DecayingMarkovChain
from decay import markov_chain_loglike, uniform_grid_loglike, DiscreteUniform


def compute_mean_logodds(args):
    (AS, path_memory), edge_dict, nodes = args
    init_dist = DiscreteUniform(len(nodes))
    mean_logodds = sum([(markov_chain_loglike(edge_dict, init_dist, path, pseudoweight=10**-5) - \
                         uniform_grid_loglike(edge_dict, init_dist, path)) 
                        for path in path_memory])
    return (AS, mean_logodds)


def main():
    ### Parse command line options
    usage = "usage: %prog [options] output_filename"
    cmdline_parser = OptionParser(usage=usage)
    cmdline_parser.add_option('-d', '--debug', dest='loglevel', default=logging.WARNING,
                              action='store_const', const=logging.DEBUG,
                              help='Debug level logging')
    cmdline_parser.add_option('--AS', dest='AS', 
                              default=None,
                              help='Set origin AS')
    cmdline_parser.add_option('--halflife', dest='halflife', 
                              action='store', type='int', default=2592000, 
                              help='Halflife of edges in seconds')
    cmdline_parser.add_option('--time-window', dest='time_window', 
                              action='store', type='int', default=4*3600, 
                              help='Length of time window in seconds')
    cmdline_parser.add_option('--include-prepend', dest='include_prepend',
                              action='store_true',
                              help='Keep redundant prepends in paths.')
    cmdline_parser.add_option('-s', '--start', dest='start', default=None)
    cmdline_parser.add_option('-e', '--end', dest='end', default=None)
    options, args = cmdline_parser.parse_args()
    remove_prepend = not options.include_prepend
    options.halflife = timedelta(seconds=options.halflife)
    out_filename = args[0]
    logging.basicConfig(level=options.loglevel, format='%(asctime)s %(message)s')
    
    # Connect to database
    connection = psycopg2.connect(host='prometheus.icasa.nmt.edu', 
                                  database='bwallin', user='bwallin')

    # Set daterange
    cursor = connection.cursor()
    if options.start:
        start = dateutil.parser.parse(options.start)
    else:
        cursor.execute('SELECT timestamp FROM announced_paths ORDER BY timestamp ASC LIMIT 1')
        start, = cursor.fetchone()
    if options.end:
        end = dateutil.parser.parse(options.end)
    else:
        cursor.execute('SELECT timestamp FROM announced_paths ORDER BY timestamp DESC LIMIT 1')
        end, = cursor.fetchone()

    # Setup worker processes
    n_workers = 8 #multiprocessing.cpu_count()
    worker_pool = multiprocessing.Pool(n_workers)

    ### Parse updates, maintaining decaying graph and periodically computing logodds over recent memory
    time_window = timedelta(seconds=options.time_window)
    time_step = timedelta(seconds=options.time_window)
    path_memory_by_origin = defaultdict(lambda: ShortTermMemory(time_window))
    graph = DecayingMarkovChain(halflife=options.halflife, learn_delay=time_window)

    logging.info('Running queuries.')
    if options.AS is not None:
        cursor = connection.cursor()
        cursor.execute('''SELECT count(*) FROM announced_paths 
                          WHERE aspath[array_upper(aspath, 1)] = %s 
                          AND timestamp BETWEEN %s AND %s''', (options.AS, start, end))
        total, = cursor.fetchone()
        cursor = connection.cursor('McServerside')
        cursor.execute('''SELECT prefixes, aspath, timestamp FROM announced_paths 
                          WHERE aspath[array_upper(aspath, 1)] = %s 
                          AND timestamp BETWEEN %s AND %s ORDER BY timestamp''', (options.AS, start, end))
    else:
        cursor = connection.cursor()
        cursor.execute('SELECT count(*) FROM announced_paths WHERE timestamp BETWEEN %s AND %s', (start, end))
        total, = cursor.fetchone()
        cursor = connection.cursor('McServerside')
        cursor.execute('''SELECT prefixes, aspath, timestamp FROM announced_paths 
                          WHERE timestamp BETWEEN %s AND %s ORDER BY timestamp''', (start, end))
    logging.info('Done.')

    # Iterate over announced messages in order
    iteration = 0
    total_iterations = floor(divtd(end-start, time_step))
    progress = ProgressBar(maxval=total_iterations, 
            widgets=[Percentage(), ' ', Bar(), ' ', ETA(), ' ', Timer()]).start()
    prev_calculation_timestamp = start - time_step/2
    logodds_series = defaultdict(lambda: zeros(total_iterations))
    timestamp_series = []
    for record in cursor:
        prefixes, path, timestamp = record
        path = tuple(path)
        
        # If window full, compute mean(logodds)
        if timestamp > prev_calculation_timestamp + 3*(time_step/2):
            logging.info('Timestamp: %s, graph size: (%d, %d), path memory: %d' % 
                         (str(timestamp), graph.number_of_nodes(), graph.number_of_edges(), 
                          sum(len(el.memory) for el in path_memory_by_origin.values())))
            calculation_timestamp = prev_calculation_timestamp + time_step
            graph.update(calculation_timestamp)
            edge_dict = dict(graph)
            for (AS, mean_logodds) in worker_pool.map(compute_mean_logodds, 
                                                      izip(path_memory_by_origin.iteritems(), 
                                                           repeat(edge_dict), repeat(graph.node_set))):
                logodds_series[AS][iteration] = mean_logodds

            timestamp_series.append(calculation_timestamp)

            edge_set = set()
            for AS in path_memory_by_origin:
                for p in path_memory_by_origin[AS]:
                    for i in xrange(len(p)-1):
                        edge_set.add((p[i], p[i+1]))

            for edge in edge_set:
                graph.weight_edge(edge, 1., calculation_timestamp)

            prev_calculation_timestamp = prev_calculation_timestamp + time_step

            iteration += 1
            progress.update(iteration)

        # Preprocess path
        if remove_prepend:
            path = tuple(remove_duplicates(path))

        # Refresh memory with announcement
        origin = path[-1]
        path_memory_by_origin[origin].learn(path, timestamp) 


    progress.finish()
    results = {'options': options,
               'logodds_series': dict(logodds_series),
               'timestamp_series': timestamp_series}
    cPickle.dump(results, open(out_filename, 'wb'))

    cPickle.dump(dict(graph), open('graph_dump', 'wb'))
 

if __name__=='__main__':
    cProfile.run('main()', 'stats_multi')
