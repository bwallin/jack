import logging
import pdb
import dateutil.parser
from optparse import OptionParser
import multiprocessing

from bgp_util import bgpfile_parser, announce_path_writer, bgpfile_generator, extract_announce_paths, QueueStop, queue_logger

def main():
    ### Parse command line options
    usage = "usage: %prog [options]"
    cmdline_parser = OptionParser(usage=usage)
    cmdline_parser.add_option('-o', '--out-file', dest='out_filename',
                              metavar='FILE',
                              help='Output filename FILE')
    cmdline_parser.add_option('-d', '--debug', dest='loglevel', default=logging.WARNING,
                              action='store_const', const=logging.DEBUG,
                              help='Debug level logging')
    cmdline_parser.add_option('-s', '--start', dest='start', default=None)
    cmdline_parser.add_option('-e', '--end', dest='end', default=None)
    options, args = cmdline_parser.parse_args()
    num_cpu = multiprocessing.cpu_count()
    num_parser_processes = 3 
    num_output_processes = num_cpu

    logging.basicConfig(level=options.loglevel, format='%(asctime)s %(message)s')
        
    if options.start:
        startdatetime = dateutil.parser.parse(options.start)
    if options.end:
        enddatetime = dateutil.parser.parse(options.end)

    # Create queues and processes
    file_queue = multiprocessing.JoinableQueue()
    record_queue = multiprocessing.JoinableQueue(5000000)

    dump_processes = [multiprocessing.Process(target=bgpfile_parser, 
                                              args=(file_queue, record_queue, extract_announce_paths)) 
                      for i in xrange(num_parser_processes)]
    output_processes = [multiprocessing.Process(target=announce_path_writer, 
                                                args=(record_queue,))
                        for i in xrange(num_output_processes)]
    queue_log_process = multiprocessing.Process(target=queue_logger, 
                                                args=(record_queue, 6))

    # Queue files
    for filename in bgpfile_generator(startdatetime, enddatetime):
        file_queue.put(filename)
    file_queue.put(QueueStop(num_parser_processes))
    
    # Start processes
    for p in dump_processes:
        p.start()
    for p in output_processes:
        p.start()
    queue_log_process.start()

    # Wait to finish
    for p in dump_processes:
        p.join()
    for p in output_processes:
        p.join()
    queue_log_process.terminate()


if __name__=='__main__':
    main()
